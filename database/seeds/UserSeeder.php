<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'id'=>1,
            'name' => 'admin',
            'email' => 'admin@test.com',
            'role'=>'admin',
            'password' => Hash::make(24681012),
        ]);
        DB::table('users')->insert([
            'id'=>2,
            'name' => 'client1',
            'email' => 'client1@test.com',
            'role'=>'client',
            'password' => Hash::make(24681012),
        ]);
        DB::table('users')->insert([
            'id'=>3,
            'name' => 'client2',
            'email' => 'client2@test.com',
            'role'=>'client',
            'password' => Hash::make(24681012),
        ]);
        DB::table('users')->insert([
            'id'=>4,
            'name' => 'reviewer',
            'email' => 'reviewer@test.com',
            'role'=>'reviewer',
            'password' => Hash::make(24681012),
        ]);
    }
}
