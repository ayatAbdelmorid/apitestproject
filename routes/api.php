<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
 //users 
Route::group(['prefix' => 'users', 'middleware' => 'auth:api'], function(){
   

Route::get('/', 'UserController@index')->middleware('role:admin');
Route::get('/show/{id}', 'UserController@show')->middleware('role:admin,client,reviewer');
Route::post('/update/{id}', 'UserController@update')->middleware('role:admin,client,reviewer');
Route::get('/destroy/{id}', 'UserController@destroy')->middleware('role:admin');
});
//articles
Route::group(['prefix' => 'articles', 'middleware' => 'auth:api'], function(){

Route::get('/', 'ArticleController@index')->middleware('role:admin,reviewer');
Route::get('/show/{id}', 'ArticleController@show')->middleware('role:admin,client,reviewer');
Route::post('/create', 'ArticleController@create')->middleware('role:admin,client');
Route::post('/update/{id}', 'ArticleController@update')->middleware('role:admin,client,reviewer');
Route::get('/destroy/{id}', 'ArticleController@destroy')->middleware('role:admin,client');
});

Route::post('login', 'UserController@login')->name('login');
Route::post('register', 'UserController@register')->name('register');
Route::post('logout', 'UserController@logout')->name('logout');

