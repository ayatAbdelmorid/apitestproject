<?php

namespace App\Http\Controllers;

use App\Article;
use Illuminate\Http\Request;
use Auth;
use Validator;
use Illuminate\Support\Facades\File; 

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::user()->role=='admin'||Auth::user()->role=='reviewer')
        {
            $articles= Article::all();
            return $articles;
        }               
        return response()->json(['error'=>'inavlid access'], 403); 

    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [ 
            'name' => 'required', 
            'file' => 'required|file', 
            'date' => 'required|date', 
            'body' => 'nullable|min:20', 
        ]);
        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }
        $input = $request->all(); 
        $article= new Article();
        $article->name=$input['name'];
        $article->date=date('Y-m-d', strtotime($input['date']));        
        $article->body=isset($input['body'])?$input['body']:'';
        $article->user_id=Auth::user()->id;

        if($request->hasfile('file')){
            $fileNameWithExt = $request->file('file')->getClientOriginalName();
            $fileName = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
            $extension = $request->file('file')->getClientOriginalExtension();
            $fileNameToStore =  $fileName.'_'.time().'.'.$extension;
            $file = $request->file;
            $file->move('uploads/',$fileNameToStore );
            $article->file=$fileNameToStore;
        }
      
        $article->save();
 
        
        return response()->json($article, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    { $article= Article::find($id);
        if(isset($article)&&(Auth::user()->role=='admin'||Auth::user()->role=='reviewer'||Auth::user()->id==$article->user_id))
        {
            
            return $article; 
        }               
        return response()->json(['error'=>'inavlid access'], 403); 

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $article = Article::findOrFail($id);
        if(isset($article)&&(Auth::user()->role=='admin'||Auth::user()->role=='reviewer'||Auth::user()->id==$article->user_id))
        {
           
            $validator = Validator::make($request->all(), [ 
                'name' => 'required',  
                'date' => 'required|date', 
                'body' => 'nullable|min:20', 
            ]);
            if ($validator->fails()) { 
                return response()->json(['error'=>$validator->errors()], 401);            
            }
            if( !isset($article->file)&&!$request->hasfile('file')){
                return response()->json(['error'=>'please add file'], 401); 
            }
            $input = $request->all(); 
            $article->name=$input['name'];
            $article->date=date('Y-m-d', strtotime($input['date']));        
            $article->body=isset($input['body'])?$input['body']:'';
            if($request->hasfile('file')){
                $fileNameWithExt = $request->file('file')->getClientOriginalName();
                $fileName = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
                $extension = $request->file('file')->getClientOriginalExtension();
                $fileNameToStore =  $fileName.'_'.time().'.'.$extension;
                $file = $request->file;
                $file->move('uploads/',$fileNameToStore );
                $article->file=$fileNameToStore;
            }
          
            $article->save();
            return response()->json($article, 200);
        }               
        return response()->json(['error'=>'inavlid access'], 403); 


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $article =Article::find($id);

        if(isset($article)&&(Auth::user()->role=='admin'||Auth::user()->id==$article->user_id) )
        
        {
            File::delete(public_path('uploads/'.$article->file));
            $article->delete();

            return response()->json(null, 204);
        }               
        return response()->json(['error'=>'inavlid access'], 403); 

    }
}
